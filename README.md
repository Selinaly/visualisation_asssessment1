# Visualtisation Assessment 1

1. Launch 3 types of instances
   - Ubuntu 
   - AWS linux
   - Load balancer (HAProxy)

Ubuntu and AWS Linux including 

`User:` instructor 

`Password:` m4g1cAut0L0t10n

sshd set to allow password login and firewall allows anywhere to ssh 
webpage should include:

- Hello there.
- My Private IP address is <system IP from ifconfig>
- Your name
- Name and Version of the operating system (obtained from the file /etc/redhat-release or /etc/os-release or /etc/lsb-release)
- The web server software the system is running and the version of the package installed.  HINT: rpm and dpkg will help.

## Things Done:
Launched all 3 instance - 
ubuntu webserver (ip 3.249.115.8)
aws webserver (ip 34.243.59.2)
ubuntu loadbalancer (ip 34.254.91.234)
Loadbalancing through haproxy

### Provisioning aws server
Install apache onto aws server
sudo -y yum install httpd
Start the apache server
sudo systemctl start httpd
Creating the html for .html file
awk 'FNR <=2' /etc/os-release
Prints just the first two lines from that file which includes the name and version of operating system
My IP address is $(ifconfig | grep -A1 eth0 | grep inet | awk '{print $2}'| sed 's/^.*://')
Prints the private ip address of this server
/usr/sbin/httpd -v | awk 'FNR <=1'
Prints the server software and the version

