#!/bin/bash
if (( $# > 0 ))
then
  hostname=$1
else
  echo "WTF: you must supply a hostname or IP address" 1>&2
  exit 1
fi

# hostname=$1
​
ssh -o StrictHostKeyChecking=no -i ~/.ssh/SelinaLyKey.pem ubuntu@$hostname '
​
sudo apt install nginx
if rpm -qa | grep "^nginx-[0-9]" >/dev/null 2>&1
then
  sudo systemctl start nginx
else
  exit 1
fi
​
sudo sh -c "cat >/var/www/html/index.nginx <<_END_
<html>
    <body>
        <h1>Hi there.<br> 
        <br>My Private IP address is <system IP from ifconfig><br>
        <br>Selina Ly<br>
        <br>Operating system<br>
        <br>Version<br></h1>
    </body>
</html>

_END_"
'