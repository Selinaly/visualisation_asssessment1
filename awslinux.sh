#!/bin/bash
if (( $# > 0 ))
then
  hostname=$1
else
  echo "WTF: you must supply a hostname or IP address" 1>&2
  exit 1
fi

# hostname=$1
​
scp -o StrictHostKeyChecking=no -i ~/.ssh/SelinaLyKey.pem index.init ec2-user@$hostname:index.init

ssh -o StrictHostKeyChecking=no -i ~/.ssh/SelinaLyKey.pem ec2-user@$hostname '
​
sudo yum -y install httpd
if rpm -qa | grep "^httpd-[0-9]" >/dev/null 2>&1
then
  sudo systemctl start httpd
else
  exit 1
fi
​
sudo sh -c "cat >/var/www/html/index.html <<_END_
<html>
    <body>
        <h1>Hi there.<br> 
        <br>My Private IP address is <system IP from ifconfig><br>
        <br>Selina Ly<br>
        <br>Operating system<br>
        <br>Version: awk 'FNR <=2' /etc/os-release<br></h1>
    </body>
</html>

_END_"
'